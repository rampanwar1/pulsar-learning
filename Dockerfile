FROM rubiklabs/pulsar-all:2.10.2-d3

USER root

COPY pulsar-io-cassandra-2.3.1.nar  /pulsar/connectors/pulsar-io-cassandra-2.3.1.nar

ENV PULSAR_HOME /pulsar