# Install python cli

pip install pulsar-client


# What is pulsarctl
pulsarctl is  command line tool to manage apache pulsar using command line.

# Download link
https://github.com/streamnative/pulsarctl

# Pulsarctl commands 
1. Add context without Auth
Command:- pulsarctl cluster add mylocal-docker-container --url http://localhost:8080 --broker-url pulsar://localhost:6650 

2 Add context with auth 

Note:- When you use tcp ingress use tcp.Host_name
Command:- pulsarctl context set cluster_name --admin-service-url https://tcp.xxxxxxxxxxxxxx:8443  --bookie-service-url pulsar+ssl://tcp.xxxxxxxxxxx:6651  --auth-plugin org.apache.pulsar.client.impl.auth.AuthenticationToken --token xxxxxxxxxxxxxxxxxxxxxxxxxxxx


3. List context 
Command:- pulsarctl context get 


4. check current context 
Command:- pulsarctl context current


5. Switch context
Command:- pulsarctl context use context_name


6. List tenants
Command:- pulsarctl tenants list 


7. List namespaces
Command:- pulsarctl namespaces list pulsar


8. List topics
Command:- pulsarctl topics list pulsar/system 


9. create persistent topic topic_name
Command:- pulsarctl topics create persistent://public/default/set 0 


10. Create non persistent topic 
Command:- pulsarctl topics create non-persistent://public/default/tmdc-storage 0


11. List subscriptions
Command:- pulsarctl subscriptions list topic_name


12. create subscriptions
Command:- pulsarctl subscriptions create topic_name subscriptions_name


13. List functions
Command:- pulsarctl functions list 


14. List sinks 
Command:- pulsarctl sinks list 


15. to get more commands
Commands:- pulsarctl --help

#======================================================#
