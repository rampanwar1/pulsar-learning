from pulsar import Client, ConsumerType

client = Client('pulsar://localhost:6650')

consumer = client.subscribe(
    topic='persistent://public/default/topic-1',
    subscription_name='mynewshared',
    consumer_type=ConsumerType.Shared
)

while True:
    try:
        msg = consumer.receive()
        print("Received message: {}".format(msg.data().decode()))
        consumer.acknowledge(msg)
    except Exception as e:
        print("Error while receiving message: {}".format(e))

client.close()
