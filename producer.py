import pulsar

client = pulsar.Client('pulsar://127.0.0.1:6650')

producer = client.create_producer('persistent://public/default/topic-1')

for i in range(200000):
    producer.send(('This is test pulsar messages No:-%d' % i).encode('utf-8'))

client.close()